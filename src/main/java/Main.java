
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

public final class Main extends Application {

    public HashMap<String, ArrayList<String>> combination = new HashMap<>();

    public String scenario;

    public String resultScenario;

    public ArrayList<String> variableName = new ArrayList<>();

    //private Desktop desktop = Desktop.getDesktop();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage stage) throws Exception {
        stage.setTitle("Test");

        final FileChooser fileChooser = new FileChooser();
        final Button openPictButton = new Button("Open PICT file");
        final Button openCucumberButton = new Button("Open Cucumber scenario");
        final Button savePairwiseScenarios = new Button("Save Scenarios");
        final Label versionLabel = new Label("©Timonov \n ver. 0.2.0 alpha");

        //buttons status
        openCucumberButton.disableProperty().setValue(true);
        savePairwiseScenarios.disableProperty().setValue(true);
        //...............

        openPictButton.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(final ActionEvent e) {
                        openPictButton.disableProperty().setValue(true);
                        openCucumberButton.disableProperty().setValue(false);
                        configureFileChooserPict(fileChooser);
                        File file = fileChooser.showOpenDialog(stage);
                        if (file != null) {
                            openPictFile(file);
                        }
                    }
                });

        openCucumberButton.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(final ActionEvent e) {
                        openCucumberButton.disableProperty().setValue(true);
                        savePairwiseScenarios.disableProperty().setValue(false);
                        configureFileChooserCucumber(fileChooser);
                        File file = fileChooser.showOpenDialog(stage);
                        if (file != null) {
                            openCucumberFile(file);
                        }
                    }
                });

        savePairwiseScenarios.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(final ActionEvent e) {
                        openPictButton.disableProperty().setValue(false);
                        savePairwiseScenarios.disableProperty().setValue(true);
                        saveCucumberScenario();
                        configureFileChooserCucumberSaver(fileChooser);
                        File file = fileChooser.showSaveDialog(stage);
                        if (file != null) {
                            try {
                                saveFile(resultScenario, file);
                            } catch (ClassNotFoundException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                });

        //
        final GridPane inputGridPanePict = new GridPane();

        GridPane.setConstraints(openPictButton, 0, 0);
        inputGridPanePict.setHgap(10);
        inputGridPanePict.setVgap(10);
        inputGridPanePict.getChildren().addAll(openPictButton);

        final GridPane inputGridPaneCucumber = new GridPane();
        GridPane.setConstraints(openCucumberButton, 0, 0);
        inputGridPaneCucumber.setHgap(10);
        inputGridPaneCucumber.setVgap(10);
        inputGridPaneCucumber.getChildren().addAll(openCucumberButton);

        final GridPane saveGridPaneCucumber = new GridPane();
        GridPane.setConstraints(savePairwiseScenarios, 0, 0);
        saveGridPaneCucumber.setHgap(10);
        saveGridPaneCucumber.setVgap(10);
        saveGridPaneCucumber.getChildren().addAll(savePairwiseScenarios);

        final GridPane labelsGridPane = new GridPane();
        GridPane.setConstraints(versionLabel,0,0);
        labelsGridPane.setHgap(10);
        labelsGridPane.setVgap(10);
        labelsGridPane.getChildren().addAll(versionLabel);


        final Pane rootGroup = new VBox(50);
        rootGroup.getChildren().addAll(inputGridPanePict, inputGridPaneCucumber, saveGridPaneCucumber,labelsGridPane);
        rootGroup.setPadding(new Insets(12, 12, 12, 12));

        stage.setScene(new Scene(rootGroup));
        stage.show();
    }

    public void openPictFile(File file) {

        try {
            POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(file));
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);
            HSSFRow row;
            HSSFCell cell;

            int rows; // No of rows
            rows = sheet.getPhysicalNumberOfRows();

            int cols = 0; // No of columns
            int tmp = 0;

            // This trick ensures that we get the data properly even if it doesn't start from first few rows
            for (int i = 0; i < 10 || i < rows; i++) {
                row = sheet.getRow(i);
                if (row != null) {
                    tmp = sheet.getRow(i).getPhysicalNumberOfCells();
                    if (tmp > cols) cols = tmp;
                }
            }

            for (int c = 0; c < cols; ++c) {
                String variable = null;
                ArrayList<String> listCombination = new ArrayList<>();
                for (int r = 0; r < rows; r++) {
                    row = sheet.getRow(r);
                    if (row != null) {
                        cell = row.getCell(c);
                        if (cell != null) {
                            if (r == 0) {
                                variable = cell.getStringCellValue();
                                variableName.add(cell.getStringCellValue());
                            } else {

                                switch (cell.getCellType()) {
                                    case Cell.CELL_TYPE_STRING:
                                        listCombination.add(cell.getStringCellValue());
                                        break;
                                    case Cell.CELL_TYPE_NUMERIC:
                                        if (DateUtil.isCellDateFormatted(cell)) {
                                            listCombination.add(String.valueOf(cell.getDateCellValue()));
                                        } else {
                                            listCombination.add(String.valueOf(cell.getNumericCellValue()));
                                        }
                                        break;
                                    case Cell.CELL_TYPE_BOOLEAN:
                                        listCombination.add(String.valueOf(cell.getBooleanCellValue()));
                                        break;
                                    case Cell.CELL_TYPE_FORMULA:
                                        listCombination.add(String.valueOf(cell.getCellFormula()));
                                        break;
                                    default:
                                        //System.out.println();
                                }
                            }
                        }
                    }

                }//r++
                combination.put(variable, listCombination);
            }//c++
        } catch (Exception ioe) {
            ioe.printStackTrace();
        }


    }

    public void openCucumberFile(File file) {

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            scenario = sb.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void saveCucumberScenario() {

        StringBuilder sb = new StringBuilder();

        int combSize = combination.get(variableName.get(0)).size();
        for (int i = 0; i < combSize; ++i) {
            String tmpScenario = scenario;
            for (int j = 0; j < combination.size(); ++j) {
                ArrayList<String> takeVariable = (ArrayList) combination.get(variableName.get(j));
                if (scenario.contains(variableName.get(j))) {
                    if (takeVariable.get(i).contains("$")){
                        tmpScenario = tmpScenario.replace("@",String.valueOf(i)).replace("<" + variableName.get(j) + ">", takeVariable.get(i).replace("$","")).replace("$"+variableName.get(j)+"$","#");
                    }
                    else if (takeVariable.get(i).contains("space")) {
                        tmpScenario = tmpScenario.replace("@",String.valueOf(i)).replace("<" + variableName.get(j) + ">", " ").replace("$" + variableName.get(j) + "$", "");
                    }
                    else if (takeVariable.get(i).contains("empty")){
                        tmpScenario = tmpScenario.replace("@",String.valueOf(i)).replace("<" + variableName.get(j) + ">", "").replace("$" + variableName.get(j) + "$", "");
                    }
                    else  tmpScenario = tmpScenario.replace("@",String.valueOf(i)).replace("<" + variableName.get(j) + ">", takeVariable.get(i)).replace("$" + variableName.get(j) + "$", "");
                }

            }
            sb.append(tmpScenario);
            sb.append(System.lineSeparator());
            sb.append(System.lineSeparator());

        }
        resultScenario = sb.toString();
    }

    private void saveFile(String content, File file) throws ClassNotFoundException {
        try {
            FileWriter fileWriter = null;

            fileWriter = new FileWriter(file);
            fileWriter.write(content);
            fileWriter.close();
        } catch (IOException ex) {
            //Logger.getLogger(Class.forName(Main.class.getName())).log(Level.SEVERE, null, ex);
        }

    }

    private static void configureFileChooserPict(final FileChooser fileChooser) {
        fileChooser.setTitle("Open PICT xls document");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Excel docs", "*.xls")
        );
    }

    private static void configureFileChooserCucumber(final FileChooser fileChooser) {
        fileChooser.setTitle("Open Cucumber Scenario document");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Text Documents", "*.txt")
        );
    }

    private static void configureFileChooserCucumberSaver(final FileChooser fileChooser) {
        fileChooser.setTitle("Save Cucumber Scenario document");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Text Documents", "*.txt")
        );
    }


}
